// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayAuxiliary.cginc" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The RayAuxiliary.cginc </summary>
// --------------------------------------------------------------------------------------------------------------------

/// <summary>The invert direction by dividation.</summary>
/// <param name="d">The direction to be inverted.</param>
/// <returns>Inverted direction.</returns>
float3 InvertDirection(float3 d)
{
    return 1.0 / d;
}

/// <summary>The get homogenous point.</summary>
/// <param name="p">The point to be converted to homogenous coordinates.</param>
/// <returns>Homogenous point.</returns>
float4 GetHomogenousPoint(float3 p)
{
    return float4(p.xyz, 1.0);
}

/// <summary>The get homogenous direction.</summary>
/// <param name="d">The direction to be converted to homogenous coordinates.</param>
/// <returns>Homogenous direction.</returns>
float4 GetHomogenousDirection(float3 d)
{
    return float4(d.xyz, 0.0);
}

/// <summary>The homogenous to cartesian conversion.</summary>
/// <param name="p">The homogenous point to be converted to cartesian coordinates.</param>
/// <returns>Cartesian point.</returns>
float4 HomToCart(float4 p)
{
    float scale = p.w;
    if (scale == 0)
    {
        scale = 1;
    }
    return p / scale;
}

/// <summary>Convert barycentric to cartesiean coordinate of the given triangle ABC (overload).</summary>
/// <param name="a">The triangle's vertex A.</param>
/// <param name="b">The triangle's vertex B.</param>
/// <param name="c">The triangle's vertex C.</param>
/// <param name="baryCoord">The barycentric point to be converted.</param>
/// <returns>Cartesian coordinate.</returns>
float2 BaryToCart(float2 a, float2 b, float2 c, float3 baryCoord)
{
    return (a * baryCoord.z) + (b * baryCoord.x) + (c * baryCoord.y);
}

/// <summary>Convert barycentric to cartesiean coordinate of the given triangle ABC (overload).</summary>
/// <param name="a">The triangle's vertex A.</param>
/// <param name="b">The triangle's vertex B.</param>
/// <param name="c">The triangle's vertex C.</param>
/// <param name="baryCoord">The barycentric point to be converted.</param>
/// <returns>Cartesian coordinate.</returns>
float3 BaryToCart(float3 a, float3 b, float3 c, float3 baryCoord)
{
    return (a * baryCoord.z) + (b * baryCoord.x) + (c * baryCoord.y);
}

/// <summary>Convert barycentric to cartesiean coordinate of the given triangle (overload).</summary>
/// <param name="tri">The triangle.</param>
/// <param name="baryCoord">The barycentric point to be converted.</param>
/// <returns>Cartesian coordinate.</returns>
float3 BaryToCart(Triangle tri, float3 baryCoord)
{
    return BaryToCart(tri.a, tri.b, tri.c, baryCoord);
}

/// <summary>Check if the passed coordinates are equal.</summary>
/// <param name="f1">The first coordinate.</param>
/// <param name="f2">The second coordinate.</param>
/// <returns>Whether the passed coordinates are equal.</returns>
bool Equals(float3 f1, float3 f2)
{
    return f1.x == f2.x && f1.y == f2.y && f1.z == f2.z;
}

/// <summary>Clip coordinate in projection space.</summary>
/// <param name="p">The point to be clipped.</param>
/// <returns>Whether the passed coordinate is clipped.</returns>
bool ClipPoint(float3 p)
{
    return (p.x >= -1 && p.x <= 1) && (p.y >= -1 && p.y <= 1) && (p.z >= -1 && p.z <= 1);
}

/// <summary>Clip coordinate in projection space (overload).</summary>
/// <param name="p">The point to be clipped.</param>
/// <returns>Whether the passed coordinate is clipped.</returns>
bool ClipPoint(float4 p)
{
    return ClipPoint(p.xyz);
}

/// <summary>The log. Writes passed argument to log buffer. Used for debugging.</summary>
/// <param name="m">The matrix to be logged.</param>
void Log(float4x4 m)
{
    logBuffer[0].logMatrix = m;
}

/// <summary>The log (overload). Writes passed argument to log buffer. Used for debugging.</summary>
/// <param name="v">The vector3 to be logged.</param>
void Log(float4 v)
{
    logBuffer[0].logVector = v;
}

/// <summary>The log (overload). Writes passed argument to log buffer. Used for debugging.</summary>
/// <param name="v">The vector4 to be logged.</param>
void Log(float3 v)
{
    Log(float4(v.xyz, 0));
}

/// <summary>The is middle pixel. Used for debugging.</summary>
/// <param name="pixelID">The pixel ID.</param>
/// <returns>Whether passed pixel ID is the middle pixel.</returns>
bool IsMiddle(uint2 pixelID)
{
    uint2 mRes = screenResolution / 2;
    return pixelID.x == mRes.x && pixelID.y == mRes.y;
}

/// <summary>The get ray.</summary>
/// <param name="id">The ID at ray origin and direction map.</param>
/// <returns>Ray at passed ID.</returns>
Ray GetRay(uint2 id)
{
    Ray ray;
    ray.origin = rayOriginMap[id];
    ray.direction = rayDirectionMap[id];
    return ray;
}

/// <summary>The set ray.</summary>
/// <param name="id">The ID at ray origin and direction map.</param>
void SetRay(uint2 id, Ray newRay)
{
    rayOriginMap[id] = newRay.origin;
    rayDirectionMap[id] = newRay.direction;
}

/// <summary>The get ray direction.</summary>
/// <param name="id">The ID at ray direction map.</param>
/// <returns>Ray direction at passed ID.</returns>
float3 GetRayDirection(uint2 id)
{
    return rayDirectionMap[id];
}

/// <summary>The set ray direction.</summary>
/// <param name="id">The ID at ray direction map.</param>
void SetRayDirection(uint2 id, float3 newDirection)
{
    rayDirectionMap[id] = newDirection;
}

/// <summary>The get ray origin.</summary>
/// <param name="id">The ID at ray origin map.</param>
/// <returns>Ray origin at passed ID.</returns>
float3 GetRayOrigin(uint2 id)
{
    return rayOriginMap[id];
}

/// <summary>The set ray origin.</summary>
/// <param name="id">The ID at ray origin map.</param>
void SetRayOrigin(uint2 id, float3 newOrigin)
{
    rayOriginMap[id] = newOrigin;
}

/// <summary>The get triangle at.</summary>
/// <param name="triangleID">The ID at triangle ID buffer.</param>
/// <returns>Triangle at passed triangle ID in camera space.</returns>
Triangle GetTriangleAt(uint triangleID)
{
    float4x4 modelCamMatrix = mul(camMatrix, modelMatrix);
    float4 aWS = float4(vertices[triangleIDs[triangleID]], 1);
    float4 bWS = float4(vertices[triangleIDs[triangleID + 1]], 1);
    float4 cWS = float4(vertices[triangleIDs[triangleID + 2]], 1);

    Triangle targetTriangle;
    targetTriangle.a = mul(modelCamMatrix, aWS).xyz;
    targetTriangle.b = mul(modelCamMatrix, bWS).xyz;
    targetTriangle.c = mul(modelCamMatrix, cWS).xyz;

    return targetTriangle;
}

/// <summary>The is ray facing triangle.</summary>
/// <param name="rayOrigin">The ray origin.</param>
/// <param name="rayDirection">The ray direction.</param>
/// <param name="triA">The triangle's vertex A.</param>
/// <param name="triB">The triangle's vertex B.</param>
/// <param name="triC">The triangle's vertex C.</param>
/// <returns>Whether the passed ray is facing towards the passed triangle.</returns>
bool IsRayFacingTriangle(float3 rayOrigin, float3 rayDirection, float3 triA, float3 triB, float3 triC)
{
    float3 oa = triA - rayOrigin;
    float3 ob = triB - rayOrigin;
    float3 oc = triC - rayOrigin;

    float dotA = dot(rayDirection, oa);
    float dotB = dot(rayDirection, ob);
    float dotC = dot(rayDirection, oc);

    return dotA >= 0 && dotB >= 0 && dotC >= 0;
}

/// <summary>The is ray facing triangle (overload).</summary>
/// <param name="ray">The ray.</param>
/// <param name="tri">The triangle.</param>
/// <returns>Whether the passed ray is facing towards the passed triangle.</returns>
bool IsRayFacingTriangle(Ray ray, Triangle tri)
{
    return IsRayFacingTriangle(ray.origin, ray.direction, tri.a, tri.b, tri.c);
}