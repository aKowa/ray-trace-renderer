// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayMath.cginc" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The RayMath.cginc </summary>
// --------------------------------------------------------------------------------------------------------------------

/// <summary>The ray triangle hit. Determines if ray and triangle hit. Writes hit coordinate into out baryCoord.</summary>
/// <param name="a">The triangle's vertex A.</param>
/// <param name="b">The triangle's vertex B.</param>
/// <param name="c">The triangle's vertex C.</param>
/// <param name="o">The ray origin.</param>
/// <param name="d">The ray direction.</param>
/// <param name="baryCoord">The out paramter of the hit point as barycentric coordinate.</param>
/// <source>Thrane, N. & Simonsen, L. O., 2005. A Comparison of Acceleration Structures for GPU Assisted Ray Tracing, Aarhus: Aarhus University, Datalogisk Institut. p.30.
/// Based on M�ller, T. & Trumbore, B., 1997. Fast, Minimum Storage Ray/Triangle Intersection. Journal of Graphics, pp. 21-28.</source>
/// <returns>Whether ray hit triangle.</returns>
bool RayTriangleHit(float3 a, float3 b, float3 c, float3 o, float3 d, out float3 baryCoord)
{
    float3 e1 = b - a;
    float3 e2 = c - a;
    float3 p = cross(d, e2);
    float det = dot(p, e1);
    float invdet = 1.0f / det;
    float3 tvec = o - a;
    float u = dot(p, tvec) * invdet;
    float3 q = cross(tvec, e1);
    float v = dot(q, d) * invdet;
    float t = dot(q, e2) * invdet;
    baryCoord.x = u;
    baryCoord.y = v;
    baryCoord.z = 1.0f - (u + v);
    return (u >= 0.0f) && (v >= 0.0f) && (u + v <= 1.0f);
}

/// <summary>The ray triangle hit (overload). Determines if ray and triangle hit.</summary>
/// <param name="a">The triangle's vertex A.</param>
/// <param name="b">The triangle's vertex B.</param>
/// <param name="c">The triangle's vertex C.</param>
/// <param name="o">The ray origin.</param>
/// <param name="d">The ray direction.</param>
/// <returns>Whether ray hit triangle.</returns>
bool RayTriangleHit(float3 a, float3 b, float3 c, float3 o, float3 d)
{
    float3 temp;
    return RayTriangleHit(a, b, c, o, d, temp);
}

/// <summary>The ray triangle hit (overload). Determines if ray and triangle hit.</summary>
/// <param name="tri">The triangle.</param>
/// <param name="ray">The ray.</param>
/// <returns>Whether ray hit triangle.</returns>
bool RayTriangleHit(Triangle tri, Ray ray)
{
    float3 temp;
    return RayTriangleHit(tri.a, tri.b, tri.c, ray.origin, ray.direction, temp);
}

/// <summary>The ray triangle hit (overload). Determines if ray and triangle hit. Writes hit coordinate into out baryCoord.</summary>
/// <param name="tri">The triangle.</param>
/// <param name="ray">The ray.</param>
/// <param name="baryCoord">The out paramter of the hit point as barycentric coordinate.</param>
/// <returns>Whether ray hit triangle.</returns>
bool RayTriangleHit(Triangle tri, Ray ray, out float3 baryCoord)
{
    return RayTriangleHit(tri.a, tri.b, tri.c, ray.origin, ray.direction, baryCoord);
}

/// <summary>The ray triangle hit (overload). Determines if ray and box intersect. NOTE: convert ray and box to the same space.</summary>
/// <param name="rayOrigin">The ray origin.</param>
/// <param name="rayDirection">The ray direction.</param>
/// <param name="boundMin">The bounding box min.</param>
/// <param name="boundMax">The bounding box max.</param>
/// <source> Barnes, T., 2011. Fast, Branchless Ray/Bounding Box Intersections. [Online] Available at : https://tavianator.com/fast-branchless-raybounding-box-intersections/ [Accessed10 02 2018].
/// <returns>Whether ray hit axis-aligned-bounding-box (AABB).</returns>
bool RayAABBHit(float3 rayOrigin, float3 rayDirection, float3 boundMin, float3 boundMax)
{
    float tmin = -1.#INF, tmax = 1.#INF;
    float dInvX = 1.0 / rayDirection.x;
    float dInvY = 1.0 / rayDirection.y;
    float dInvZ = 1.0 / rayDirection.z;

    float tX1 = (boundMin.x - rayOrigin.x) * dInvX;
    float tX2 = (boundMax.x - rayOrigin.x) * dInvX;
    tmin = max(tmin, min(tX1, tX2));
    tmax = min(tmax, max(tX1, tX2));
    
    float tY1 = (boundMin.y - rayOrigin.y) * dInvY;
    float tY2 = (boundMax.y - rayOrigin.y) * dInvY;
    tmin = max(tmin, min(tY1, tY2));
    tmax = min(tmax, max(tY1, tY2));
   
    float tZ1 = (boundMin.z - rayOrigin.z) * dInvZ;
    float tZ2 = (boundMax.z - rayOrigin.z) * dInvZ;
    tmin = max(tmin, min(tZ1, tZ2));
    tmax = min(tmax, max(tZ1, tZ2));
    
    return tmax >= max(tmin, 0.0);
}