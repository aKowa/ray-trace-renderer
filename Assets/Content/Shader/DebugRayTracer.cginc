class DebugColorGradient
{
    float4 PrimaryRayPass(uint2 pixelID)
    {
        float x = (float) pixelID.x / (float) screenResolution.x;
        float y = (float) pixelID.y / (float) screenResolution.y;
        return float4(x, y, 0, 1);
    }
};