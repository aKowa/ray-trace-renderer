// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BasicRayTracer.cginc" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The BasicRayTracer.cginc </summary>
// --------------------------------------------------------------------------------------------------------------------

#include "KernelDataGPU.cginc"
#include "RayMath.cginc"
#include "RayAuxiliary.cginc"

/// <summary>The set primary ray. Sets initial origin and calculates normalized primary ray direction.</summary>
/// <param name="pixelID">The pixel ID.</param>
void SetPrimaryRay(uint2 pixelID)
{
    Ray targetRay;
    targetRay.origin = float3(0, 0, 0);
    
    float tX = (float) pixelID.x / (float) screenResolution.x;
    float tY = (float) pixelID.y / (float) screenResolution.y;
    float dX = lerp(camLowerCorner.x, camUpperCorner.x, tX);
    float dY = lerp(camLowerCorner.y, camUpperCorner.y, tY);
    targetRay.direction = normalize(float3(dX, dY, -camLowerCorner.z));
    SetRay(pixelID, targetRay);
}

/// <summary>The get bound hit. Determines if passed ray hit the current objects bounds.</summary>
/// <param name="origin">The ray origin.</param>
/// <param name="direction">The ray direction.</param>
/// <returns>If passed ray hit the current object.</returns>
bool GetBoundHit(float3 origin, float3 direction)
{
    // convert ray to model space
    float4x4 modelCamMatrixInv = mul(modelMatrixInv, camMatrixInv);
    float4 rayOriginMS = mul(modelCamMatrixInv, float4(origin, 1));
    float4 rayDirectionMS = mul(modelCamMatrixInv, float4(direction, 0));
    
    // determine AABB hit
    return RayAABBHit(rayOriginMS.xyz, rayDirectionMS.xyz, objectBoundMin, objectBoundMax);
}

/// <summary>The get bound hit (overload). Determines if passed ray hit the current objects bounds.</summary>
/// <param name="ray">The ray.</param>
/// <returns>If passed ray hit the current object.</returns>
bool GetBoundHit(Ray ray)
{
    return GetBoundHit(ray.origin, ray.direction);
}

/// <summary>The get primary ray hit. Determines if ray at pixel ID hits the triangle and updates depth accordingly.</summary>
/// <param name="pixelID">The pixel ID.</param>
/// <param name="triangleID">The triangle ID.</param>
/// <param name="sharedDepthID">The shared depth ID.</param>
/// <returns>Whether primary ray at pixel ID hit the triangle.</returns>
bool GetPrimaryRayHit(uint2 pixelID, uint triangleID, uint sharedDepthID)
{
    Triangle tri = GetTriangleAt(triangleID);
    float3 triNormal = tri.GetNormal();
    Ray ray = GetRay(pixelID);
    
    float s = dot(ray.direction, triNormal); 
    if (s > 0 || !enableCulling) // check culling
    {
        float3 outHitCoordBary = float3(0, 0, 0);
        bool hasHitTriangle = RayTriangleHit(tri, ray, outHitCoordBary);   
        if (hasHitTriangle)
        {
            float3 hitCoordCS = BaryToCart(tri, outHitCoordBary);
            float depth = length(hitCoordCS);

            depthShared[sharedDepthID] = asuint(depthMap[pixelID]);
            GroupMemoryBarrier(); // sync threads        
            InterlockedMin(depthShared[sharedDepthID], asuint(depth)); // locked variable access

            if (depth == asfloat(depthShared[sharedDepthID])) // check depth and assign new parameter
            {                
                depthMap[pixelID] = depth;
                hitMap[pixelID] = hitCoordCS;
                hitNormalMap[pixelID] = triNormal;
                // targetTexture[pixelID] = float4(outHitCoordBary, 1); // Use this barycentric coordinate for texture sampling
                return true;
            }
        }
    }
    return false;
}

/// <summary>The get shadow ray hit. Calculates shadow ray at pixel ID and determines if it hit any triangle.</summary>
/// <param name="pixelID">The pixel ID.</param>
/// <returns>Whether shadow ray at pixel ID hit any triangle.</returns>
bool GetShadowRayHit(uint2 pixelID)
{
    float3 lightDirectionWS = lightBuffer[0].direction; // light direction in world space
    float3 lightDirectionCS = normalize(mul(camMatrix, float4(lightDirectionWS.xyz, 0)).xyz); // light direction in camera space
    
    Ray shadowRay;
    shadowRay.origin = hitMap[pixelID];
    shadowRay.direction = lightDirectionCS * -1.0f; // flip light direction
    
    bool hasHitBound = GetBoundHit(shadowRay);
    if (hasHitBound)
    {
        // traverse triangles
        uint size;
        uint stride;
        triangleIDs.GetDimensions(size, stride);
        
        for (int i = 0; i < (int) size; i += 3)
        {
            Triangle tri = GetTriangleAt(i);
            
            bool isFacing = IsRayFacingTriangle(shadowRay, tri); // check if triangle is in shadow ray direction
            if (isFacing)
            {
                bool hasHitTriangle = RayTriangleHit(tri, shadowRay);
                if (hasHitTriangle)
                {
                    return true;
                }
            }
        }
    }
    return false;
}