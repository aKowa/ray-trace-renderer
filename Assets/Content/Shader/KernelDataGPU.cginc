// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KernelDataGPU.cginc" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The KernelDataGPU.cginc </summary>
// --------------------------------------------------------------------------------------------------------------------

/// <summary>The resolution group thread x and y dimensions.</summary>
#define resGroupDimXY 32

/// <summary>The group thread y dimension used for pixel buffer.</summary>
#define hitGroupSizeY 8

/// <summary>The log data used for debugging.</summary>
struct LogData
{
    float4x4 logMatrix;
    float4 logVector;
    uint2 resolution;
    int rayNum;
    int rayDiscardBound;
    int rayDiscardTriangle;
    int rayHitBound;
    int rayHitTriangle;
};

/// <summary>The light data.</summary>
struct LightData
{
    float4 color;
    float3 direction;
    float intensity;
};

/// <summary>The ray.</summary>
struct Ray
{
    float3 origin;
    float3 direction;
};


/// <summary>The triangle.</summary>
struct Triangle
{
    float3 a;
    float3 b;
    float3 c;
    
    /// <summary>The get normal.</summary>
	/// <returns>Surface normal of this triangle.</returns>
    float3 GetNormal()
    {
        float3 e1 = b - a;
        float3 e2 = c - a;
        return normalize(cross(e1, e2));
    }
    
    /// <summary>Converts passed point from barycentric to cartesian coordiante system, relative to this triangle.</summary>
	/// <param name="baryPoint">The barycentric point to be converted.</param>
	/// <returns>Passed point in cartesian coordinate system.</returns>
    float3 BaryToCart(float3 baryPoint)
    {
        return (a * baryPoint.z) + (b * baryPoint.x) + (c * baryPoint.y);
    }
};

/// <summary>The color red. Used for debugging.</summary>
static const float4 red = float4(1, 0, 0, 1);

/// <summary>The color green. Used for debugging.</summary>
static const float4 green = float4(0, 1, 0, 1);

/// <summary>The color blue. Used for debugging.</summary>
static const float4 blue = float4(0, 0, 1, 1);


/// <summary>The constant camera buffer.</summary>
cbuffer ConstantCameraBuffer : register(b0)
{
    /// <summary>The camera/view matrix.</summary>
    float4x4 camMatrix;

    /// <summary>The inverse camera matrix.</summary>
    float4x4 camMatrixInv;

    /// <summary>The background color.</summary>
    float4 cameraBackgroundColor;

    /// <summary>The world camera position.</summary>
    float3 cameraPositionWorld;

    /// <summary>The camera's (view frustum) lower corner.</summary>
    float3 camLowerCorner;

    /// <summary>The camera's (view frustum) upper corner.</summary>
    float3 camUpperCorner;

    /// <summary>The camera's screen resolution.</summary>
    uint2 screenResolution;
};


/// <summary>The constant model buffer.</summary>
cbuffer ConstantModelBuffer : register(b1)
{
    /// <summary>The model matrix.</summary>
    float4x4 modelMatrix;
    
    /// <summary>The inverse model matrix.</summary>
    float4x4 modelMatrixInv;

    /// <summary>The object color.</summary>
    float4 objectColor;

    /// <summary>The object bound min.</summary>
    float3 objectBoundMin;

    /// <summary>The object bound max.</summary>
    float3 objectBoundMax;

    /// <summary>The enable culling bool flag.</summary>
    bool enableCulling;
};

/// <summary>The log buffer used for debugging.</summary>
RWStructuredBuffer<LogData> logBuffer;

/// <summary>The light buffer.</summary>
StructuredBuffer<LightData> lightBuffer;

/// <summary>The object's main texture.</summary>
Texture2D<float4> mainTexture;

/// <summary>The target texture.</summary>
RWTexture2D<float4> targetTexture;

/// <summary>The hit map.</summary>
RWTexture2D<float3> hitMap;

/// <summary>The hit surface normal map.</summary>
RWTexture2D<float3> hitNormalMap;

/// <summary>The ray origin map.</summary>
RWTexture2D<float3> rayOriginMap;

/// <summary>The ray direction map.</summary>
RWTexture2D<float3> rayDirectionMap;

/// <summary>The object's vertices.</summary>
StructuredBuffer<float3> vertices;

/// <summary>The input pixel buffer.</summary>
StructuredBuffer<uint2> inPixelBuffer;

/// <summary>The output pixel buffer.</summary>
AppendStructuredBuffer<uint2> outPixelBuffer;

/// <summary>The object's triangle IDs.</summary>
StructuredBuffer<int> triangleIDs;

/// <summary>The depth map.</summary>
RWTexture2D<float> depthMap;

/// <summary>The groupshared depth buffer.</summary>
groupshared uint depthShared[hitGroupSizeY];

/// <summary>The basic linear clamp sampler. Used for texturing.</summary>
SamplerState BasicLinearClampSampler;
