﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayTraceRenderPipelineAsset.cs" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 03/2018 </copyright>
// <summary> The RayTraceRenderPipelineAsset.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEditor;

using UnityEngine;
using UnityEngine.Experimental.Rendering;

/// <summary>The ray trace render pipeline asset.</summary>
[ExecuteInEditMode]
public class RayTraceRenderPipelineAsset : RenderPipelineAsset
{
	/// <summary>The private ray trace compute shader.</summary>
	[SerializeField]
	private ComputeShader rayTraceCS;

	/// <summary>Gets the singleton ray trace compute shader.</summary>
	public ComputeShader RayTraceCS
	{
		get
		{
			return this.rayTraceCS;
		}
	}

#if UNITY_EDITOR

	[MenuItem("RayTraceRenderPipelineAsset/Create RayTraceRenderPipeline")]
	private static void CreateRayTraceRenderPipeline()
	{
		var instance = CreateInstance<RayTraceRenderPipelineAsset>();
		AssetDatabase.CreateAsset(instance, "Assets/Content/Test/RayTraceRenderPipeline.asset");
	}

#endif

	/// <summary>The internal create pipeline.</summary>
	/// <returns>The <see cref="IRenderPipeline"/>.</returns>
	protected override IRenderPipeline InternalCreatePipeline()
	{
		return new RayTraceRenderInstance(this);
	}
}