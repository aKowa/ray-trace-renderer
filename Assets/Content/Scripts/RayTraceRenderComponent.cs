﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayTraceRenderComponent.cs" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The RayTraceRenderComponent.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;

/// <summary>The append log. Used to determine what should be logged.</summary>
public enum AppendLog
{
	/// <summary>No logging required.</summary>
	None = -1,

	/// <summary>Log amount of rays that hit the object.</summary>
	Rays = 0,

	/// <summary>Log amount of triangles that each ray needs to check against.</summary>
	Triangles = 1
}

/// <summary>
/// The ray trace renderer class. Base class for each game object that should be rendered via ray tracing (Equivalent to Unity's Mesh Renderer).
/// </summary>
[ExecuteInEditMode]
public class RayTraceRenderComponent : MonoBehaviour
{
	/// <summary>The pixel buffer argument size (uint * 2).</summary>
	private const int sizeOfArgs = sizeof(uint) * 2;

	/// <summary>The size of a triangle id (int).</summary>
	private const int sizeOfTriangleID = sizeof(int);

	/// <summary>The size of a vertex (float3).</summary>
	private const int sizeOfVertex = sizeof(float) * 3;

	/// <summary>This game object's colour.</summary>
	[Tooltip("This game object's colour.")]
	public Color color;

	/// <summary>Enables culling for this game object.</summary>
	[Tooltip("Enables culling for this game object.")]
	public bool enableCulling;

	/// <summary>Enables this object to cast shadows.</summary>
	[Tooltip("Enables this object to cast shadows.")]
	public bool enableShadows;

	/// <summary>This game object's mesh.</summary>
	[Tooltip("This game object's mesh.")]
	public Mesh mesh;

	/// <summary>The append log used for debugging.</summary>
	[Tooltip("The append log used for debugging.")]
	[SerializeField]
	private AppendLog appendLog = AppendLog.None;

	/// <summary>Gets a value indicating whether this game object can be rendered checking for null and alpha colour.</summary>
	private bool CanBeRendered
	{
		get
		{
			return this.mesh && this.color.a > 0.0f;
		}
	}

	/// <summary>Subscribes game object's functions to render events.</summary>
	private void OnEnable()
	{
		RayTraceManager.primaryRenderUpdate += this.OnPrimaryRenderUpdate;
		RayTraceManager.shadowRenderUpdate += this.OnShadowRenderUpdate;
	}

	/// <summary>Removes game object's functions from render events.</summary>
	private void OnDisable()
	{
		RayTraceManager.primaryRenderUpdate -= this.OnPrimaryRenderUpdate;
		RayTraceManager.shadowRenderUpdate -= this.OnShadowRenderUpdate;
	}

	/// <summary>Sets game object's bound parameter in passed shader context. Needed for pre-render bound check.</summary>
	/// <param name="shader">The shader context.</param>
	private void SetBoundParam(ComputeShader shader)
	{
		shader.SetMatrix(KernelParam.modelMatrix, this.transform.localToWorldMatrix);
		shader.SetMatrix(KernelParam.modelMatrixInv, this.transform.worldToLocalMatrix);
		shader.SetVector(KernelParam.objectBoundMin, this.mesh.bounds.min);
		shader.SetVector(KernelParam.objectBoundMax, this.mesh.bounds.max);
		shader.SetVector(KernelParam.objectColor, this.color);
	}

	/// <summary>Sets game object's model parameter in passed shader context. Needed for primary ray pass.</summary>
	/// <param name="shader">The shader context.</param>
	private void SetModelParam(ComputeShader shader, out ComputeBuffer vertexBuffer, out ComputeBuffer triangleIDBuffer)
	{
		shader.SetBool(KernelParam.enableCulling, this.enableCulling);

		vertexBuffer = new ComputeBuffer(this.mesh.vertices.Length, sizeOfVertex);
		vertexBuffer.SetData(this.mesh.vertices);
		shader.SetBuffer(RayTraceManager.shadowPassID, KernelParam.vertices, vertexBuffer);
		shader.SetBuffer(RayTraceManager.primaryPassID, KernelParam.vertices, vertexBuffer);

		triangleIDBuffer = new ComputeBuffer(this.mesh.triangles.Length, sizeOfTriangleID);
		triangleIDBuffer.SetData(this.mesh.triangles);
		shader.SetBuffer(RayTraceManager.shadowPassID, KernelParam.triangleIDs, triangleIDBuffer);
		shader.SetBuffer(RayTraceManager.primaryPassID, KernelParam.triangleIDs, triangleIDBuffer);
	}

	/// <summary>Logs the triangle of this game object's mesh at the passed ID.</summary>
	/// <param name="triangleID">The triangle id to be logged.</param>
	private void LogTriangleAt(int triangleID)
	{
		var a = this.mesh.vertices[this.mesh.triangles[triangleID]];
		var b = this.mesh.vertices[this.mesh.triangles[triangleID + 1]];
		var c = this.mesh.vertices[this.mesh.triangles[triangleID + 2]];

		Debug.Log("Triangle ID: " + triangleID + "   A: " + a + "   B: " + b + "   C: " + c);
	}

	/// <summary>The primary render update function. (Un-)Subscribed in OnEnable/ OnDisable.</summary>
	/// <param name="camera">The camera to be rendered.</param>
	/// <param name="shader">The shader context.</param>
	private void OnPrimaryRenderUpdate(Camera camera, ComputeShader shader)
	{
		if (this.CanBeRendered)
		{
			// check bounds pass
			this.SetBoundParam(shader);

			var bufferSize = camera.pixelWidth * camera.pixelHeight;
			var boundHitPixelIDBuffer = new ComputeBuffer(bufferSize, sizeOfArgs, ComputeBufferType.Append);
			boundHitPixelIDBuffer.SetCounterValue(0);
			shader.SetBuffer(RayTraceManager.boundPassID, KernelParam.outPixelBuffer, boundHitPixelIDBuffer);

			var boundThreadSize = RayTraceManager.GetResolutionThreadGroupSize(camera) / RayTraceManager.threadGroupSizes.boundCheck;
			shader.Dispatch(RayTraceManager.boundPassID, boundThreadSize);

			// primary ray pass
			ComputeBuffer vertexBuffer;
			ComputeBuffer triangleIDBuffer;
			this.SetModelParam(shader, out vertexBuffer, out triangleIDBuffer);

			var triangleThreads = Mathf.CeilToInt((float)this.mesh.triangles.Length / 3 / RayTraceManager.threadGroupSizes.primaryRay.y);
			var args = new[] { 0, triangleThreads, 1 }; // prepare triangle's thread group size on Y so that each triangle corresponds to one thread
			var argBuffer = new ComputeBuffer(args.Length, sizeof(int), ComputeBufferType.IndirectArguments);
			argBuffer.SetData(args);
			ComputeBuffer.CopyCount(boundHitPixelIDBuffer, argBuffer, 0); // copy amount in pixel buffer to argument buffer (no read back to CPU)

			shader.SetBuffer(RayTraceManager.primaryPassID, KernelParam.inPixelBuffer, boundHitPixelIDBuffer);

			shader.DispatchIndirect(RayTraceManager.primaryPassID, argBuffer); // dispatch creating one thread for each hit pixel and each triangle

			// Debugging
			if (this.appendLog != AppendLog.None)
			{
				argBuffer.GetData(args);
				Debug.Log(this.name + "   | Dispatch Indirect |   " + args[(int)this.appendLog] + " " + this.appendLog);
			}

			// Release buffers
			argBuffer.Release();
			boundHitPixelIDBuffer.Release();
			vertexBuffer.Release();
			triangleIDBuffer.Release();
		}
	}

	/// <summary>The shadow render update function. (Un-)Subscribed in OnEnable/ OnDisable.</summary>
	/// <param name="camera">The rendered camera.</param>
	/// <param name="shader">The shader context.</param>
	private void OnShadowRenderUpdate(Camera camera, ComputeShader shader)
	{
		if (this.enableShadows && this.CanBeRendered)
		{
			this.SetBoundParam(shader);
			ComputeBuffer vertexBuffer;
			ComputeBuffer triangleIDBuffer;
			this.SetModelParam(shader, out vertexBuffer, out triangleIDBuffer);

			var shadowThreadSize = RayTraceManager.GetResolutionThreadGroupSize(camera) / RayTraceManager.threadGroupSizes.boundCheck;
			shader.Dispatch(RayTraceManager.shadowPassID, shadowThreadSize);

			vertexBuffer.Release();
			triangleIDBuffer.Release();
		}
	}
}