﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AntiAliasingLevel.cs" company="PolyPaw"> Andre Kowalewski, 12/2017 </copyright>
// <summary> The AntiAliasingLevel.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

public enum AntiAliasingLevel
{
	One = 1,

	Two = 2,

	Four = 4,

	Eight = 8
}