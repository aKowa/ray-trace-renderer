﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayTraceManager.cs" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The RayTraceManager.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

using UnityEngine;

using Object = UnityEngine.Object;

/// <summary>
/// The ray trace manager. Hooked into Unity's scriptable rendering pipeline, distributing rendering calls and containing auxiliary functions.
/// </summary>
public static class RayTraceManager
{
	/// <summary>The post render update event.</summary>
	public static RenderUpdate postRenderUpdate;

	/// <summary>The pre render update event.</summary>
	public static RenderUpdate preRenderUpdate;

	/// <summary>The primary render update event.</summary>
	public static RenderUpdate primaryRenderUpdate;

	/// <summary>The render initialize event.</summary>
	public static RenderInit renderInit;

	/// <summary>The secondary render update event.</summary>
	public static RenderUpdate secondaryRenderUpdate;

	/// <summary>The shadow render update event.</summary>
	public static RenderUpdate shadowRenderUpdate;

	/// <summary>The ID of the BoundCheck kernel.</summary>
	internal static int boundPassID;

	/// <summary>The ID of the Initialize kernel.</summary>
	internal static int initialPassID;

	/// <summary>The ID of the LightRay kernel.</summary>
	internal static int lightPassID;

	/// <summary>The ID of the PrimaryRay kernel.</summary>
	internal static int primaryPassID;

	/// <summary>The ray trace compute shader used as rendering context and for dispatching draw calls.</summary>
	internal static ComputeShader rayTraceCS;

	/// <summary>The ID of the ShadowRay kernel.</summary>
	internal static int shadowPassID;

	/// <summary>Data hub containing all kernels thread group sizes.</summary>
	internal static ThreadGroupSizes threadGroupSizes;

	/// <summary>The target render texture. GPU writes into this texture. This texture is drawn to the screen.</summary>
	private static RenderTexture targetTexture;

	/// <summary>The render initialize delegate.</summary>
	/// <param name="shader">The shader context.</param>
	public delegate void RenderInit(ComputeShader shader);

	/// <summary>The render update delegate.</summary>
	/// <param name="camera">The camera to be rendered.</param>
	/// <param name="shader">The shader context.</param>
	public delegate void RenderUpdate(Camera camera, ComputeShader shader);

	/// <summary>Convert camera resolution to ThreadGroupSize. Divide by kernel's ThreadGroupSize to get final size.</summary>
	/// <param name="camera">The camera to get the resolution from.</param>
	/// <returns>Target ThreadGroupSize.</returns>
	internal static ThreadGroupSize GetResolutionThreadGroupSize(Camera camera)
	{
		return new ThreadGroupSize(camera.pixelWidth, camera.pixelHeight, 1);
	}

	/// <summary>Create temporary render texture usable by a compute shader.</summary>
	/// <param name="width">The render textures width.</param>
	/// <param name="height">The render textures height.</param>
	/// <param name="format">The render textures format.</param>
	/// <returns>Temporary render texture with random write enabled.</returns>
	private static RenderTexture CreateTexture(int width, int height, RenderTextureFormat format = RenderTextureFormat.Default)
	{
		var newTexture = RenderTexture.GetTemporary(width, height, 0, format);
		newTexture.enableRandomWrite = true;
		newTexture.Create();
		return newTexture;
	}

	/// <summary>Calculate camera corners and assign them to out arguments.</summary>
	/// <param name="camera">The camera to get the corners from.</param>
	/// <param name="lower">The lower left corner.</param>
	/// <param name="upper">The upper right corner.</param>
	private static void GetViewCorners(Camera camera, out Vector3 lower, out Vector3 upper)
	{
		var frustumHeight = 2.0f * camera.nearClipPlane * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
		var frustumWidth = frustumHeight * camera.aspect;
		upper = new Vector3(frustumWidth / 2, frustumHeight / 2, camera.nearClipPlane);
		lower = new Vector3(upper.x * -1, upper.y * -1, camera.nearClipPlane);
	}

	/// <summary>Initial renderer setup. Call on construction.</summary>
	/// <param name="newRayTraceCS">The new ray trace compute shader.</param>
	public static void SetupRenderer(ComputeShader newRayTraceCS)
	{
		rayTraceCS = newRayTraceCS;

		// Find Kernel IDs
		initialPassID = rayTraceCS.FindKernel(KernelPass.Initialize);
		boundPassID = rayTraceCS.FindKernel(KernelPass.BoundCheck);
		primaryPassID = rayTraceCS.FindKernel(KernelPass.PrimaryRay);
		shadowPassID = rayTraceCS.FindKernel(KernelPass.ShadowRay);
		lightPassID = rayTraceCS.FindKernel(KernelPass.LightRay);

		// Initialize kernel thread group sizes
		threadGroupSizes = new ThreadGroupSizes(rayTraceCS, initialPassID, boundPassID, primaryPassID, shadowPassID, lightPassID);

		if (renderInit != null)
		{
			renderInit.Invoke(rayTraceCS);
		}
	}

	/// <summary>Main entry point for ray trace render loop.</summary>
	/// <param name="cameras">The cameras to be rendered.</param>
	public static void Render(IEnumerable<Camera> cameras)
	{
		foreach (var camera in cameras)
		{
			var w = camera.pixelWidth;
			var h = camera.pixelHeight;

			// set camera data to constant buffer
			Vector3 lowerCorner;
			Vector3 upperCorner;
			GetViewCorners(camera, out lowerCorner, out upperCorner);
			rayTraceCS.SetVector(KernelParam.camLowerCorner, lowerCorner);
			rayTraceCS.SetVector(KernelParam.camUpperCorner, upperCorner);
			rayTraceCS.SetMatrix(KernelParam.camMatrix, camera.worldToCameraMatrix);
			rayTraceCS.SetMatrix(KernelParam.camMatrixInv, camera.cameraToWorldMatrix);
			rayTraceCS.SetVector(KernelParam.cameraPositionWorld, camera.transform.position);
			rayTraceCS.SetVector(KernelParam.cameraBackgroundColor, camera.backgroundColor);
			rayTraceCS.SetInts(KernelParam.screenResolution, camera.pixelWidth, camera.pixelHeight);

			// set primary ray maps
			var rayOriginMap = CreateTexture(w, h, RenderTextureFormat.ARGBFloat);
			rayTraceCS.SetTexture(initialPassID, KernelParam.rayOriginMap, rayOriginMap);
			rayTraceCS.SetTexture(boundPassID, KernelParam.rayOriginMap, rayOriginMap);
			rayTraceCS.SetTexture(primaryPassID, KernelParam.rayOriginMap, rayOriginMap);
			rayTraceCS.SetTexture(lightPassID, KernelParam.rayOriginMap, rayOriginMap);

			var rayDirectionMap = CreateTexture(w, h, RenderTextureFormat.ARGBFloat);
			rayTraceCS.SetTexture(initialPassID, KernelParam.rayDirectionMap, rayDirectionMap);
			rayTraceCS.SetTexture(boundPassID, KernelParam.rayDirectionMap, rayDirectionMap);
			rayTraceCS.SetTexture(primaryPassID, KernelParam.rayDirectionMap, rayDirectionMap);
			rayTraceCS.SetTexture(lightPassID, KernelParam.rayDirectionMap, rayDirectionMap);

			// set target texture
			targetTexture = CreateTexture(w, h);
			rayTraceCS.SetTexture(initialPassID, KernelParam.targetTexture, targetTexture);
			rayTraceCS.SetTexture(boundPassID, KernelParam.targetTexture, targetTexture);
			rayTraceCS.SetTexture(primaryPassID, KernelParam.targetTexture, targetTexture);
			rayTraceCS.SetTexture(shadowPassID, KernelParam.targetTexture, targetTexture);
			rayTraceCS.SetTexture(lightPassID, KernelParam.targetTexture, targetTexture);

			// set depth map
			var depthMap = CreateTexture(w, h, RenderTextureFormat.RFloat);
			rayTraceCS.SetTexture(initialPassID, KernelParam.depthMap, depthMap);
			rayTraceCS.SetTexture(primaryPassID, KernelParam.depthMap, depthMap);

			// set hit record maps
			var hitMap = CreateTexture(w, h, RenderTextureFormat.ARGBFloat);
			rayTraceCS.SetTexture(primaryPassID, KernelParam.hitMap, hitMap);
			rayTraceCS.SetTexture(shadowPassID, KernelParam.hitMap, hitMap);
			rayTraceCS.SetTexture(lightPassID, KernelParam.hitMap, hitMap);

			var hitNormalMap = CreateTexture(w, h, RenderTextureFormat.ARGBFloat);
			rayTraceCS.SetTexture(primaryPassID, KernelParam.hitNormalMap, hitNormalMap);
			rayTraceCS.SetTexture(shadowPassID, KernelParam.hitNormalMap, hitNormalMap);
			rayTraceCS.SetTexture(lightPassID, KernelParam.hitNormalMap, hitNormalMap);

			// Dispatch initial pass
			var initialThreadGroupSize = GetResolutionThreadGroupSize(camera) / threadGroupSizes.initialize;
			rayTraceCS.Dispatch(initialPassID, initialThreadGroupSize);

			// Invoke draw calls on each subscribed component (renderer, lights,...)
			preRenderUpdate.SafeInvoke(camera, rayTraceCS);
			primaryRenderUpdate.SafeInvoke(camera, rayTraceCS);
			shadowRenderUpdate.SafeInvoke(camera, rayTraceCS);
			secondaryRenderUpdate.SafeInvoke(camera, rayTraceCS);
			postRenderUpdate.SafeInvoke(camera, rayTraceCS);

			// redefine screen rectangle for preview camera
			var rect = new Rect(0, 0, w, h);
			if (camera.name == "Preview Camera")
			{
				rect = new Rect(0, 20, w, h);
			}

			// render target texture to screen
			Graphics.DrawTexture(rect, targetTexture);

			// release temporary render textures to reduce garbage collection
			rayOriginMap.Release();
			rayDirectionMap.Release();
			targetTexture.Release();
			depthMap.Release();
			hitMap.Release();
			hitNormalMap.Release();
		}
	}
}