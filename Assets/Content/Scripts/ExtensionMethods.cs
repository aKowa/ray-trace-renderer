﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtensionMethods.cs" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The ExtensionMethods.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

using UnityEngine;

/// <summary>The extension methods.</summary>
public static class ExtensionMethods
{
	/// <summary>The safe invoke. Checks null before invocation.</summary>
	/// <param name="del">The delegate to be safely invoked.</param>
	public static void SafeInvoke(this Delegate del)
	{
		if (del != null)
		{
			del.DynamicInvoke();
		}
	}

	/// <summary>The safe invoke (overload). Checks null before invocation.</summary>
	/// <param name="del">The delegate to be safely invoked</param>
	/// <param name="args">The arguments this delegate should be invoked with.</param>
	public static void SafeInvoke(this Delegate del, params object[] args)
	{
		if (del != null)
		{
			del.DynamicInvoke(args);
		}
	}

	/// <summary>The dispatch. Dispatch compute shader kernel with passed thread group size.</summary>
	/// <param name="shader">The shader to be dispatched.</param>
	/// <param name="kernelID">The kernel id to be dispatched.</param>
	/// <param name="threadGroupSize">The thread group size to be dispatched.</param>
	public static void Dispatch(this ComputeShader shader, int kernelID, ThreadGroupSize threadGroupSize)
	{
		shader.Dispatch(kernelID, (int)threadGroupSize.x, (int)threadGroupSize.y, (int)threadGroupSize.z);
	}
}