﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayTraceLightComponent.cs" company="PolyPaw"> Andre Kowalewski, 01/2018 </copyright>
// <summary> The RayTraceLightComponent.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

using UnityEngine;

/// <summary>The light data.</summary>
[Serializable]
public struct LightData
{
	/// <summary>The light colour. 16 byte.</summary>
	public Color colour;

	/// <summary>The light direction. 12 byte.</summary>
	[HideInInspector]
	public Vector3 direction;

	/// <summary>The light intensity. 4 byte.</summary>
	public float intensity;
}

/// <summary>The ray trace light component.</summary>
[ExecuteInEditMode]
public class RayTraceLightComponent : MonoBehaviour
{
	/// <summary>The size of light data in byte.</summary>
	private const int sizeOfLightData = 32;

	/// <summary>The light data.</summary>
	public LightData lightData = new LightData { colour = Color.white, intensity = 1 };

	/// <summary>The light data buffer.</summary>
	private ComputeBuffer lightDataBuffer;

	/// <summary>The on enable. Executed when this component is enabled.</summary>
	private void OnEnable()
	{
		// Debug.Log(name + " Enabled");
		RayTraceManager.preRenderUpdate += this.OnPreRenderUpdate;
		RayTraceManager.secondaryRenderUpdate += this.OnSecondaryRenderUpdate;
	}

	/// <summary>The on disable. Executed when this component is disabled.</summary>
	private void OnDisable()
	{
		// Debug.Log(name + " Disabled");
		RayTraceManager.preRenderUpdate -= this.OnPreRenderUpdate;
		RayTraceManager.secondaryRenderUpdate -= this.OnSecondaryRenderUpdate;
	}

	/// <summary>The on pre render update.</summary>
	/// <param name="camera">The camera.</param>
	/// <param name="shader">The compute shader.</param>
	private void OnPreRenderUpdate(Camera camera, ComputeShader shader)
	{
		// set light data
		this.lightData.direction = this.transform.forward;

		var lightDataArray = new[] { this.lightData };

		// assign light buffer
		this.lightDataBuffer = new ComputeBuffer(lightDataArray.Length, sizeOfLightData);
		this.lightDataBuffer.SetData(lightDataArray);
		shader.SetBuffer(RayTraceManager.shadowPassID, KernelParam.lightBuffer, this.lightDataBuffer);
		shader.SetBuffer(RayTraceManager.lightPassID, KernelParam.lightBuffer, this.lightDataBuffer);
	}

	/// <summary>The on secondary render update.</summary>
	/// <param name="camera">The camera.</param>
	/// <param name="shader">The compute shader.</param>
	private void OnSecondaryRenderUpdate(Camera camera, ComputeShader shader)
	{
		var boundThreadSize = RayTraceManager.GetResolutionThreadGroupSize(camera) / RayTraceManager.threadGroupSizes.boundCheck;
		shader.Dispatch(RayTraceManager.lightPassID, boundThreadSize);

		this.lightDataBuffer.Dispose();
	}
}