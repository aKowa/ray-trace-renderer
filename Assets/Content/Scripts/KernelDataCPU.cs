﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KernelDataCPU.cs" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 02/2018 </copyright>
// <summary> The KernelDataCPU.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;

/// <summary>The thread group size.</summary>
public struct ThreadGroupSize
{
	/// <summary>The thread group's x dimension.</summary>
	public uint x;

	/// <summary>The thread group's y dimension.</summary>
	public uint y;

	/// <summary>The thread group's z dimension.</summary>
	public uint z;

	/// <summary>Initializes a new instance of the <see cref="ThreadGroupSize"/> struct.</summary>
	/// <param name="newX">The new x dimension.</param>
	/// <param name="newY">The new y dimension.</param>
	/// <param name="newZ">The new z dimension.</param>
	public ThreadGroupSize(int newX, int newY, int newZ)
	{
		this.x = (uint)newX;
		this.y = (uint)newY;
		this.z = (uint)newZ;
	}

	/// <summary>Initializes a new instance of the <see cref="ThreadGroupSize"/> struct.</summary>
	/// <param name="newX">The new x dimension.</param>
	/// <param name="newY">The new y dimension.</param>
	/// <param name="newZ">The new z dimension.</param>
	public ThreadGroupSize(uint newX, uint newY, uint newZ)
	{
		this.x = newX;
		this.y = newY;
		this.z = newZ;
	}

	/// <summary>Initializes a new instance of the <see cref="ThreadGroupSize"/> struct.</summary>
	/// <param name="shader">The shader holding target kernel.</param>
	/// <param name="kernelID">The kernel id to get x, y and z dimensions from.</param>
	public ThreadGroupSize(ComputeShader shader, int kernelID)
	{
		shader.GetKernelThreadGroupSizes(kernelID, out this.x, out this.y, out this.z);
	}

	/// <summary>Divides two thread group sizes. Ceils target dimensions.</summary>
	/// <param name="t1">The numerator thread group.</param>
	/// <param name="t2">The denominator thread group.</param>
	/// <returns>Divided thread group.</returns>
	public static ThreadGroupSize operator /(ThreadGroupSize t1, ThreadGroupSize t2)
	{
		var newX = GetCeilValue(t1.x, t2.x);
		var newY = GetCeilValue(t1.y, t2.y);
		var newZ = GetCeilValue(t1.z, t2.z);
		return new ThreadGroupSize(newX, newY, newZ);
	}

	/// <summary>The get ceil value.</summary>
	/// <param name="v1">The numerator.</param>
	/// <param name="v2">The denominator.</param>
	/// <returns>The divided and ceiled values as <see cref="uint"/>.</returns>
	private static uint GetCeilValue(float v1, float v2)
	{
		return (uint)Mathf.CeilToInt(v1 / v2);
	}
}

/// <summary>The thread group sizes. Containing all kernels thread group sizes.</summary>
public struct ThreadGroupSizes
{
	/// <summary>The bound check kernel thread group size.</summary>
	public ThreadGroupSize boundCheck;

	/// <summary>The initialize kernel thread group size.</summary>
	public ThreadGroupSize initialize;

	/// <summary>The light ray kernel thread group size.</summary>
	public ThreadGroupSize lightRay;

	/// <summary>The primary kernel thread group size.</summary>
	public ThreadGroupSize primaryRay;

	/// <summary>The shadow ray kernel thread group size.</summary>
	public ThreadGroupSize shadowRay;

	/// <summary>Initializes a new instance of the <see cref="ThreadGroupSizes"/> struct.</summary>
	/// <param name="shader">The shader to get the kernels from.</param>
	/// <param name="initialPassID">The initial pass id.</param>
	/// <param name="boundPassID">The bound pass id.</param>
	/// <param name="primaryPassID">The primary pass id.</param>
	/// <param name="shadowPassID">The shadow pass id.</param>
	/// <param name="lightPassID">The light pass id.</param>
	public ThreadGroupSizes(
		ComputeShader shader,
		int initialPassID,
		int boundPassID,
		int primaryPassID,
		int shadowPassID,
		int lightPassID)
	{
		this.initialize = new ThreadGroupSize(shader, initialPassID);
		this.boundCheck = new ThreadGroupSize(shader, boundPassID);
		this.primaryRay = new ThreadGroupSize(shader, primaryPassID);
		this.shadowRay = new ThreadGroupSize(shader, shadowPassID);
		this.lightRay = new ThreadGroupSize(shader, lightPassID);
	}
}

/// <summary>The kernel pass. Holding the kernel names. Used to set parameter to specific kernels.</summary>
public static class KernelPass
{
	/// <summary>The bound check kernel name.</summary>
	public const string BoundCheck = "BoundCheck";

	/// <summary>The initialize kernel name.</summary>
	public const string Initialize = "Initialize";

	/// <summary>The light ray kernel name.</summary>
	public const string LightRay = "LightRay";

	/// <summary>The primary ray kernel name.</summary>
	public const string PrimaryRay = "PrimaryRay";

	/// <summary>The shadow ray kernel name.</summary>
	public const string ShadowRay = "ShadowRay";
}

/// <summary>The kernel parameter names. Holding parameter names usable by all kernels. Used to set parameter to specific kernels.</summary>
public static class KernelParam
{
	/// <summary>The camera background colour parameter name.</summary>
	public const string cameraBackgroundColor = "cameraBackgroundColor";

	/// <summary>The camera world position parameter name.</summary>
	public const string cameraPositionWorld = "cameraPositionWorld";

	/// <summary>The camera's lower corner parameter name.</summary>
	public const string camLowerCorner = "camLowerCorner";

	/// <summary>The camera/ view matrix parameter name.</summary>
	public const string camMatrix = "camMatrix";

	/// <summary>The inverse camera matrix parameter name.</summary>
	public const string camMatrixInv = "camMatrixInv";

	/// <summary>The camera's upper corner parameter name.</summary>
	public const string camUpperCorner = "camUpperCorner";

	/// <summary>The depth map parameter name.</summary>
	public const string depthMap = "depthMap";

	/// <summary>The enable culling parameter name.</summary>
	public const string enableCulling = "enableCulling";

	/// <summary>The hit map parameter name.</summary>
	public const string hitMap = "hitMap";

	/// <summary>The hit normal map parameter name.</summary>
	public const string hitNormalMap = "hitNormalMap";

	/// <summary>The input pixel buffer parameter name.</summary>
	public const string inPixelBuffer = "inPixelBuffer";

	/// <summary>The light buffer parameter name.</summary>
	public const string lightBuffer = "lightBuffer";

	/// <summary>The log buffer parameter name.</summary>
	public const string logBuffer = "logBuffer";

	/// <summary>The main texture parameter name.</summary>
	public const string mainTexture = "mainTexture";

	/// <summary>The model matrix parameter name.</summary>
	public const string modelMatrix = "modelMatrix";

	/// <summary>The inverse model matrix parameter name.</summary>
	public const string modelMatrixInv = "modelMatrixInv";

	/// <summary>The object bound max parameter name.</summary>
	public const string objectBoundMax = "objectBoundMax";

	/// <summary>The object bound min parameter name.</summary>
	public const string objectBoundMin = "objectBoundMin";

	/// <summary>The object colour parameter name.</summary>
	public const string objectColor = "objectColor";

	/// <summary>The out pixel buffer parameter name.</summary>
	public const string outPixelBuffer = "outPixelBuffer";

	/// <summary>The ray direction map parameter name.</summary>
	public const string rayDirectionMap = "rayDirectionMap";

	/// <summary>The ray origin map parameter name.</summary>
	public const string rayOriginMap = "rayOriginMap";

	/// <summary>The screen resolution parameter name.</summary>
	public const string screenResolution = "screenResolution";

	/// <summary>The target texture parameter name.</summary>
	public const string targetTexture = "targetTexture";

	/// <summary>The triangle IDs parameter name.</summary>
	public const string triangleIDs = "triangleIDs";

	/// <summary>The vertices parameter name.</summary>
	public const string vertices = "vertices";
}