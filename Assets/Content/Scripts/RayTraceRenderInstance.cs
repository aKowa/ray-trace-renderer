﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayTraceRenderInstance.cs" company="PolyPaw"> Andre Kowalewski, 11/2017 </copyright>
// <summary> The RayTraceRenderInstance.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Experimental.Rendering;

/// <summary>The ray trace render instance used by scriptable rendering.</summary>
public class RayTraceRenderInstance : RenderPipeline
{
	/// <summary>The asset render asset to be used</summary>
	private RayTraceRenderPipelineAsset asset;

	/// <summary>Initializes a new instance of the <see cref="RayTraceRenderInstance"/> class.</summary>
	/// <param name="newAsset">The new render asset.</param>
	public RayTraceRenderInstance(RayTraceRenderPipelineAsset newAsset)
	{
		this.asset = newAsset;
		
		// call manager setup function on construction
		RayTraceManager.SetupRenderer(this.asset.RayTraceCS);
	}

	/// <summary>The render update entry point. Overrides render of render pipeline.</summary>
	/// <param name="renderContext">The render context.</param>
	/// <param name="cameras">The cameras.</param>
	public override void Render(ScriptableRenderContext renderContext, Camera[] cameras)
	{
		RayTraceManager.Render(cameras);
	}
}