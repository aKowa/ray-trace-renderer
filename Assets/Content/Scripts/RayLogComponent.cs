﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RayLogComponent.cs" company="Hamburg University of Applied Sciences"> Andre Kowalewski, 03/2018 </copyright>
// <summary> The RayLogComponent.cs </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

using UnityEngine;

/// <summary>The log data.</summary>
[Serializable]
public struct LogData
{
	/// <summary>The log matrix. 64 byte.</summary>
	public Matrix4x4 logMatrix;

	/// <summary>The log vector. 16 byte.</summary>
	public Vector4 logVector;

	/// <summary>The resolution. 8 byte</summary>
	public Vector2Int resolution;

	/// <summary>The ray num. 4 byte</summary>
	public int rayNum;

	/// <summary>The ray discard bound. 4 byte</summary>
	public int rayDiscardBound;

	/// <summary>The ray discard triangle. 4 byte.</summary>
	public int rayDiscardTriangle;

	/// <summary>The ray hit bound. 4 byte</summary>
	public int rayHitBound;

	/// <summary>The ray hit triangle. 4 byte.</summary>
	public int rayHitTriangle;
}

/// <summary>The ray log component.</summary>
[ExecuteInEditMode]
public class RayLogComponent : MonoBehaviour
{
	/// <summary>The size of log data in bytes.</summary>
	private const int sizeOfLogData = 108;

	/// <summary>The instance.</summary>
	private static RayLogComponent instance;

	/// <summary>The log data.</summary>
	public LogData logData;

	/// <summary>The delta time ms.</summary>
	[SerializeField]
	private float deltaTimeMS;

	/// <summary>The log.</summary>
	private LogData[] log = { new LogData() };

	private ComputeBuffer logBuffer;

	/// <summary>Gets the instance.</summary>
	public static RayLogComponent Instance
	{
		get
		{
			return instance ?? (instance = FindObjectOfType<RayLogComponent>());
		}
	}

	/// <summary>The internal unity update.</summary>
	private void Update()
	{
		this.deltaTimeMS = Time.deltaTime * 1000;
	}

	/// <summary>The on enable. Executed when this component is enabled.</summary>
	private void OnEnable()
	{
		RayTraceManager.preRenderUpdate += this.OnPreRenderUpdate;
		RayTraceManager.postRenderUpdate += this.OnPostRenderUpdate;
	}

	/// <summary>The on disable. Executed when this component is disabled.</summary>
	private void OnDisable()
	{
		RayTraceManager.preRenderUpdate -= this.OnPreRenderUpdate;
		RayTraceManager.postRenderUpdate -= this.OnPostRenderUpdate;
	}

	/// <summary>The on pre render update.</summary>
	/// <param name="camera">The camera.</param>
	/// <param name="shader">The compute shader.</param>
	private void OnPreRenderUpdate(Camera camera, ComputeShader shader)
	{
		this.logBuffer = new ComputeBuffer(1, sizeOfLogData);
		this.log[0] = new LogData();
		this.logBuffer.SetData(this.log);
		RayTraceManager.rayTraceCS.SetBuffer(RayTraceManager.initialPassID, KernelParam.logBuffer, this.logBuffer);
		RayTraceManager.rayTraceCS.SetBuffer(RayTraceManager.boundPassID, KernelParam.logBuffer, this.logBuffer);
		RayTraceManager.rayTraceCS.SetBuffer(RayTraceManager.primaryPassID, KernelParam.logBuffer, this.logBuffer);
		RayTraceManager.rayTraceCS.SetBuffer(RayTraceManager.shadowPassID, KernelParam.logBuffer, this.logBuffer);
		RayTraceManager.rayTraceCS.SetBuffer(RayTraceManager.lightPassID, KernelParam.logBuffer, this.logBuffer);
	}

	/// <summary>The on post render update.</summary>
	/// <param name="camera">The camera.</param>
	/// <param name="shader">The compute shader.</param>
	private void OnPostRenderUpdate(Camera camera, ComputeShader shader)
	{
		// GPU read back! Slows performance!
		this.logBuffer.GetData(this.log);
		this.logData = this.log[0];
		this.logBuffer.Dispose();

		Debug.Log("Depth: " + this.logData.logVector + "  hit Bound: " + this.logData.rayHitBound + "    hit Triangle: " + this.logData.rayHitTriangle);
	}
}